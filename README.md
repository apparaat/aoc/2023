# aoc-2023

My solutions to the [2023 Advent of Code](https://adventofcode.com/2023) challenge.

### Note

1. These scripts are not written for clarity, readability, scalability, none of the -ilities
2. These scripts are written to show completion of the challenge

